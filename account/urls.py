from django.urls import path

from .views import SignUpView, VerifyPhoneNumber, LogInView, LogOutView, ForgotPasswordView, \
    VerifyResetPasswordView, ResetPasswordView

urlpatterns = [
    path('signup/', SignUpView.as_view()),
    path('verify/', VerifyPhoneNumber.as_view()),
    path('login/', LogInView.as_view()),
    path('logout/', LogOutView.as_view()),
    path("recover/", ForgotPasswordView.as_view(), name="recover"),
    path("verify-password/", VerifyResetPasswordView.as_view()),
    path("reset-password/", ResetPasswordView.as_view()),

]