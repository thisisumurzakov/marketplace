import uuid

from django.contrib.auth import get_user_model
from django.contrib.auth.hashers import make_password
from django.core.cache import cache
from rest_framework import status
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework_simplejwt.exceptions import TokenError
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from rest_framework_simplejwt.tokens import RefreshToken
from rest_framework_simplejwt.views import TokenObtainPairView

from .models import User
from .serializers import SignUpSerializer, VerifySMSCodeSerializer, LogOutSerializer, ForgotPasswordSerializer, \
    ResetPasswordSerializer


def generate_code():
    return 1111


def send_sms(phone, code):
    return True


class SignUpView(APIView):
    throttle_scope = "sendSMS"
    permission_classes = (AllowAny,)

    def post(self, request):
        serializer = SignUpSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = serializer.validated_data
        phone_number = data["phone_number"]
        if get_user_model().objects.filter(
            phone_number=phone_number
        ).exists() or cache.get(f"{phone_number}"):
            return Response(
                data=f"{phone_number} is taken.",
                status=status.HTTP_409_CONFLICT,
            )
        code = generate_code()
        response = send_sms(phone_number, code)
        if not response:
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        data["password"] = make_password(data["password"])
        cache.set(f"{phone_number}", "taken", timeout=300)
        cache.set(f"{phone_number}_{code}", data, timeout=300)
        return Response(status=status.HTTP_201_CREATED)


class VerifyPhoneNumber(APIView):
    permission_classes = (AllowAny,)

    def post(self, request):
        serializer = VerifySMSCodeSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = serializer.validated_data
        cache_data = cache.get(f"{data['phone_number']}_{data['code']}")
        if cache_data:
            user = User.objects.create(**cache_data)
            user.save()
            cache.expire(f"{data['phone_number']}_{data['code']}", timeout=1)
            cache.expire(f"{data['phone_number']}", timeout=1)
            return Response(status=status.HTTP_200_OK)
        return Response(status=status.HTTP_406_NOT_ACCEPTABLE)


class LogInSerializer(TokenObtainPairSerializer):
    def validate(self, attrs):
        attrs = super(LogInSerializer, self).validate(attrs)
        attrs.update({'id': self.user.id})
        return attrs


class LogInView(TokenObtainPairView):
    serializer_class = LogInSerializer


class LogOutView(APIView):
    pass


class ForgotPasswordView(APIView):
    throttle_scope = "sendSMS"
    permission_classes = (AllowAny,)

    def post(self, request):
        serializer = ForgotPasswordSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = serializer.validated_data
        try:
            instance = get_user_model().objects.get(
                phone_number=data["phone_number"]
            )
        except Exception:
            return Response(
                status=status.HTTP_401_UNAUTHORIZED,
                data="No phone number found",
            )
        code = generate_code()
        response = send_sms(instance.phone_number, code)
        if not response:
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        cache.set(instance.phone_number, code, timeout=300)
        return Response(status=status.HTTP_200_OK)


class VerifyResetPasswordView(APIView):
    permission_classes = (AllowAny,)

    def post(self, request):
        serializer = VerifySMSCodeSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = serializer.validated_data
        try:
            instance = get_user_model().objects.get(
                phone_number=data["phone_number"]
            )
        except Exception:
            return Response(
                status=status.HTTP_401_UNAUTHORIZED,
                data="No phone number found",
            )
        cache_code = cache.get(instance.phone_number)
        if cache_code != data["code"]:
            return Response(status=status.HTTP_406_NOT_ACCEPTABLE)
        guid = uuid.uuid4()
        cache.set(f"{guid}", instance.phone_number, timeout=300)
        cache.expire(instance.phone_number, timeout=1)
        return Response(data={"guid": guid}, status=status.HTTP_200_OK)


class ResetPasswordView(APIView):
    permission_classes = (AllowAny,)

    def post(self, request):
        serializer = ResetPasswordSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = serializer.validated_data
        phone_number = cache.get(data["guid"])
        if phone_number is None:
            return Response(status=status.HTTP_400_BAD_REQUEST)
        instance = get_user_model().objects.get(phone_number=phone_number)
        instance.set_password(data["password"])
        instance.save()
        cache.expire(data["guid"], timeout=1)
        return Response(status=status.HTTP_200_OK)