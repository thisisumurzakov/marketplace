from django.contrib.auth import get_user_model
from rest_framework import serializers

class SignUpSerializer(serializers.Serializer):
    first_name = serializers.CharField(max_length=150)
    last_name = serializers.CharField(max_length=150)
    phone_number = serializers.CharField(max_length=100)
    password = serializers.CharField()

    def create(self, validated_data):
        return get_user_model().objects.create(**validated_data)


class VerifySMSCodeSerializer(serializers.Serializer):
    phone_number = serializers.CharField(max_length=100)
    code = serializers.IntegerField()


class LogOutSerializer(serializers.Serializer):
    refresh = serializers.CharField()


class ForgotPasswordSerializer(serializers.Serializer):
    phone_number = serializers.CharField()


class ResetPasswordSerializer(serializers.Serializer):
    password = serializers.CharField()
    guid = serializers.UUIDField()
